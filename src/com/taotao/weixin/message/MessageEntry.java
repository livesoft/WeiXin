package com.taotao.weixin.message;

/**
 * 消息参数常量
 */
public class MessageEntry {
    public static final String FromUserName = "FromUserName";
    public static final String ToUserName = "ToUserName";
    public static final String MsgType = "MsgType";
    public static final String Event = "Event";
}
